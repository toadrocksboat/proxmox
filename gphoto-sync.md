# gphoto-sync setup

1. created a new ubuntu 18.04 container

2. couldn't install snap packages. Resulted in something like this:

```
	# snap install hello-world
	error: system does not fully support snapd: cannot mount squashfs image using
	"squashfs": mount: /tmp/sanity-mountpoint-612465744: mount failed: Operation
	not permitted.
	```
3. Needed to add this line to the $vmid.conf 

	```
	features: fuse=1,mount=fuse,nesting=1
	```

4. That got a little further but now I have an apparmor error.

	```
	root@photosync:~# snap install gphotos-sync
	error: cannot perform the following tasks:
	- Setup snap "snapd" (8542) security profiles (cannot setup profiles for snap "snapd": cannot create host snap-confine apparmor configuration: cannot reload snap-confine apparmor profile: cannot load apparmor profiles: exit status 243
	apparmor_parser output:
	apparmor_parser: Unable to replace "mount-namespace-capture-helper".  Permission denied; attempted to load a profile while confined?
	apparmor_parser: Unable to replace "/snap/snapd/8542/usr/lib/snapd/snap-confine".  Permission denied; attempted to load a profile while confined?
	)
	root@photosync:~# 
	```

### References

[Can't install snap in lxc container](https://forum.proxmox.com/threads/cant-install-snap-in-lxc-container.68708/)
