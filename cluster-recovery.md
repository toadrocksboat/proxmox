# TIFU

I changed the network of my nodes without taking any precautions. 
Whoops.

I updated the IP address from 192.168.1.10/24 to 192.168.0.10/24

Now of course the nodes cannot talk to the NFS storage or each other.

We get this:

Error: cluster not ready – no quorum? (500).

And eventually over SSH I get access denied when trying to make changes or anything even as root.

```
root@pve1:~# touch /etc/pve/test
touch: cannot touch '/etc/pve/test': Permission denied


Here's my cluster config: /etc/pve/corosync.conf

	```
	logging {
	  debug: off
	  to_syslog: yes
	}

	nodelist {
	  node {
	    name: pve1
	    nodeid: 1
	    quorum_votes: 1
	    ring0_addr: 192.168.1.10
	  }
	  node {
	    name: pve2
	    nodeid: 2
	    quorum_votes: 1
	    ring0_addr: 192.168.1.20
	  }
	}

	quorum {
	  provider: corosync_votequorum
	}

	totem {
	  cluster_name: clusterbutt
	  config_version: 2
	  interface {
	    linknumber: 0
	  }
	  ip_version: ipv4-6
	  secauth: on
	  version: 2
	}
	```

## Solution

[See: Separate A Node Without Reinstalling](https://pve.proxmox.com/wiki/Cluster_Manager#_remove_a_cluster_node)

```
Ensure all shared resources are cleanly separated! Otherwise you will run into conflicts and problems.

First stop the corosync and the pve-cluster services on the node:

	systemctl stop pve-cluster
	systemctl stop corosync

Start the cluster filesystem again in local mode:

	pmxcfs -l

Delete the corosync configuration files:

	rm /etc/pve/corosync.conf
	rm /etc/corosync/*

You can now start the filesystem again as normal service:

	killall pmxcfs
	systemctl start pve-cluster

The node is now separated from the cluster. You can deleted it from a remaining node of the cluster with:

	pvecm delnode oldnode

If the command failed, because the remaining node in the cluster lost quorum when the now separate node exited, you may set the expected votes to 1 as a workaround:

	pvecm expected 1

And then repeat the pvecm delnode command.

Now switch back to the separated node, here delete all remaining files left from the old cluster. This ensures that the node can be added to another cluster again without problems.

	rm /var/lib/corosync/*
	systemctl stop corosync

Start the cluster filesystem again in local mode:

	pmxcfs -l

Delete the corosync configuration files:

	rm /etc/pve/corosync.conf
	rm /etc/corosync/*

You can now start the filesystem again as normal service:

	killall pmxcfs
	systemctl start pve-cluster

The node is now separated from the cluster. You can deleted it from a remaining node of the cluster with:

	pvecm delnode oldnode

If the command failed, because the remaining node in the cluster lost quorum when the now separate node exited, you may set the expected votes to 1 as a workaround:

	pvecm expected 1

And then repeat the pvecm delnode command.

Now switch back to the separated node, here delete all remaining files left from the old cluster. This ensures that the node can be added to another cluster again without problems.

	rm /var/lib/corosync/*
```
