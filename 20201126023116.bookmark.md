# OPNsense Firewall Rule Cheat Sheet - Home Network Guy (homenetworkguy.com)

<https://homenetworkguy.com/how-to/firewall-rules-cheat-sheet/#allow-remote-access-to-web-server-on-vlan-10-using-nat-port-forwarding>

## Description

Introduction When looking up information on how to write firewall rules in OPNsense, you may be looking for specific examples on how to block or allow certain types of network traffic rather than how to write firewall rules in general. This is especially true once you become more experienced and comfortable with writing rules. I thought it would be a good idea to consolidate a variety of scenarios into a single how-to that could be used as a quick reference guide.

## Content

![Home Network Guy](/img/homenetworkguy-shield.png) [](/ "Home Network Guy"){.logo__link}

Home Network Guy

Going beyond the basics in home networking

Menu

-   [How To\'s](/how-to){.menu__link}
-   [Not To\'s](/not-to){.menu__link}
-   [Tech](/tech){.menu__link}
-   [Reviews](/review){.menu__link}
-   [Forum](https://forum.homenetworkguy.com){.menu__link}
-   [About](/about){.menu__link}
-   [Privacy](/privacy-policy){.menu__link}

OPNsense Firewall Rule \"Cheat Sheet\" {#opnsense-firewall-rule-cheat-sheet .post__title}
======================================

A quick guide to creating firewall rules in various situations

February 07, 2020

[Firewalls](/categories/firewalls/){.meta__link}

![](/how-to/firewall-rules-cheat-sheet/arts-build-close-up-commerce-273230.jpg)

Page content

-   [Introduction](#introduction)
-   [LAN/VLAN Rules](#lanvlan-rules)
    -   [Block a single device on VLAN 10 from accessing the Internet](#block-a-single-device-on-vlan-10-from-accessing-the-internet)
    -   [Block all devices on VLAN 10 from accessing the Internet](#block-all-devices-on-vlan-10-from-accessing-the-internet)
    -   [Block all devices on VLAN 10 from accessing a single host/server on the Internet](#block-all-devices-on-vlan-10-from-accessing-a-single-hostserver-on-the-internet)
    -   [Allow a single device on VLAN 10 to access any port of single device on VLAN 20](#allow-a-single-device-on-vlan-10-to-access-any-port-of-single-device-on-vlan-20)
    -   [Allow any device on VLAN 10 to access any port of single device on VLAN 20](#allow-any-device-on-vlan-10-to-access-any-port-of-single-device-on-vlan-20)
    -   [Allow any device to access any network or device (local networks and Internet)](#allow-any-device-to-access-any-network-or-device-local-networks-and-internet)
    -   [Allow DNS requests to only be resolved by OPNsense and not by external DNS servers](#allow-dns-requests-to-only-be-resolved-by-opnsense-and-not-by-external-dns-servers)
    -   [Allow ICMP messages on all internal networks to aid in troubleshooting](#allow-icmp-messages-on-all-internal-networks-to-aid-in-troubleshooting)
-   [WAN Rules](#wan-rules)
    -   [Allow remote access on WAN to VPN server on OPNsense](#allow-remote-access-on-wan-to-vpn-server-on-opnsense)
    -   [Allow remote access to web server on VLAN 10 using NAT port forwarding](#allow-remote-access-to-web-server-on-vlan-10-using-nat-port-forwarding)
-   [Suggestions/Feedback](#suggestionsfeedback)

Introduction
------------

When looking up information on how to write firewall rules in [OPNsense](https://opnsense.org/), you may be looking for specific examples on how to block or allow certain types of network traffic rather than how to write firewall rules in general. This is especially true once you become more experienced and comfortable with writing rules. I thought it would be a good idea to consolidate a variety of scenarios into a single how-to that could be used as a quick reference guide.

It is worth noting that any IP addresses used in the examples could be substituted with aliases. If you plan to create several rules for a particular device, you may want to make sure of aliases since it allows you to quickly change the values for a bunch of rules at the same time. It also helps make the rules more readable since you do not have to remember that 192.168.10.10 is your laptop, PC, Raspberry Pi, etc. To add a new alias, go to the "Firewall \> Aliases" page.

To add new firewall rules for your various network interfaces, go to the "Firewall \> Rules" page. You will see a list of interfaces in which you may add firewall rules.

LAN/VLAN Rules
--------------

Below are some scenarios for creating firewall rules for your LAN/VLAN interface(s):

### Block a single device on VLAN 10 from accessing the Internet

If you need to block Internet (and also local network) access for a particular device on VLAN 10:

  Option             Value
  ------------------ --------------------------
  Action             Block
  Interface          VLAN10
  Protocol           any
  Source             192.168.10.10
  Source Port        any
  Destination        any
  Destination Port   any
  Description        Block access to Internet

### Block all devices on VLAN 10 from accessing the Internet

To block all devices on the entire VLAN 10 network, simply do not add any firewall rules for the VLAN 10 interface. By default, all outgoing traffic is blocked to both the Internet and other VLANs so this rule would be redundant. However for the purposes of illustration and learning, the following rule would block all outgoing traffic:

  Option             Value
  ------------------ --------------------------
  Action             Block
  Interface          VLAN10
  Protocol           any
  Source             VLAN10 net
  Source Port        any
  Destination        any
  Destination Port   any
  Description        Block access to Internet

### Block all devices on VLAN 10 from accessing a single host/server on the Internet

If you want to block access for all devices on VLAN 10 from accessing one particular IP address on the Internet:

  Option             Value
  ------------------ -----------------------------------------
  Action             Block
  Interface          VLAN10
  Protocol           any
  Source             VLAN10 net
  Source Port        any
  Destination        8.8.8.8
  Destination Port   any
  Description        Block access to the host/server 8.8.8.8

### Allow a single device on VLAN 10 to access any port of single device on VLAN 20

This rule allows a device on VLAN 10 to access any open service that is running on the device that resides on VLAN 20:

  Option             Value
  ------------------ -------------------------------
  Action             Pass
  Interface          VLAN10
  Protocol           any
  Source             192.168.10.10
  Source Port        any
  Destination        192.168.20.10
  Destination Port   any
  Description        Allow device to device access

### Allow any device on VLAN 10 to access any port of single device on VLAN 20

This rule allows every device on VLAN 10 to access any open service that is running on the device that resides on VLAN 20:

  Option             Value
  ------------------ --------------------------------
  Action             Pass
  Interface          VLAN10
  Protocol           any
  Source             VLAN10
  Source Port        any
  Destination        192.168.20.10
  Destination Port   any
  Description        Allow VLAN 10 access to device

### Allow any device to access any network or device (local networks and Internet)

This is the default "allow all" rule created by OPNsense on the LAN interface when you install OPNsense.

  Option             Value
  ------------------ ------------------------------------
  Action             Pass
  Interface          LAN
  Protocol           any
  Source             any
  Source Port        any
  Destination        any
  Destination Port   any
  Description        Allow access to any network/device

> If you use this rule, I would recommend restricting as much network traffic as possible above this rule to minimize unwanted traffic. When you have more restrictive rules above this rule, it essentially becomes a "allow all other" rule to allow any traffic you have not explicitly blocked to pass through. This is probably more useful in a home network environment so you do not have to constantly tweak your firewall rules to allow every different type of traffic through the firewall (unless you enjoy that sort of thing). Companies and the government may want to default to "deny all other" to be more restrictive in which case you would simply not include this "allow all" rule at the bottom of the rule list.

### Allow DNS requests to only be resolved by OPNsense and not by external DNS servers

You need two rules to accomplish this. I am going to assume there will be other rules below the "block all other DNS" rule. If you do not have any other rules below the first rule, by default everything else will be blocked since no rules will be triggered. You can repeat the below rules in each VLAN or alternatively, you can create these as floating rules or create a firewall rule group as described after the two rules below. For a single interface, enter the following values:

  Option             Value
  ------------------ -----------------------------------------------
  Action             Pass
  Interface          LAN
  Protocol           TCP/UDP
  Source             LAN net
  Source Port        any
  Destination        192.168.1.1
  Destination Port   53 (DNS)
  Description        Allow DNS requests to be resolved on OPNsense

  Option             Value
  ------------------ ------------------------------------------------------
  Action             Block
  Interface          LAN
  Protocol           TCP/UDP
  Source             LAN net
  Source Port        any
  Destination        any
  Destination Port   53 (DNS)
  Description        Block all other DNS requests (internal and external)

If you choose to use floating rules (located on the "Firewall \> Rules \> Floating" page), the main difference between rules defined for a particular interface and floating rules is that you can select multiple interfaces that the rules should be applied. It allows you to create a single rule that can be set on multiple interfaces. Floating rules are processed before the rule groups and interface rules. The rules above would look like the following:

  Option      Value
  ----------- ---------------
  Action      Pass
  Interface   LAN, DMZ, IOT
  ...         ...

  Option      Value
  ----------- ---------------
  Action      Block
  Interface   LAN, DMZ, IOT
  ...         ...

Alternatively, if you want to organize sets of rules, you could create a firewall rule group by going to the "Firewall \> Groups" page. As mentioned prior, group rules are processed after floating rules but before the interface rules. So you could create a group such as "DNSRules" and create the DNS rules in that group. Like with floating rules, you can select all of the interfaces in which the group of rules are applied when you create the new group. The nice thing about rule groups is that you do not have to select all of the interfaces for every new rule you create within the group because you have already defined the interfaces when you created the group. With the floating rules, you have to select all of the appropriate interfaces every time you create a new rule. The above rules would look as follows in a rule group (notice the group name as the interface):

  Option      Value
  ----------- ----------
  Action      Pass
  Interface   DNSRules
  ...         ...

  Option      Value
  ----------- ----------
  Action      Block
  Interface   DNSRules
  ...         ...

### Allow ICMP messages on all internal networks to aid in troubleshooting

Many administrators disable all ICMP messages on the network both internal and external since they can be abused. However, allowing pings and destination unreachable messages on your internal network can be helpful with troubleshooting connectivity issues on your network.

If you have multiple VLANs, I recommend that you create a firewall rule group or use floating rules. For the firewall group or the float rules, you would select only the LAN/VLAN interfaces in which you wish to use ICMP. Then you can create the rules below. The example below is using the group name ICMPGroup as the interface (since the group already knows what interfaces to apply the rules). If you using floating rules instead, you can select the appropriate list of LAN/VLAN interfaces instead of using the group name as the interface. These are the most common and useful ICMP messages:

  Option        Value
  ------------- ----------------------------------
  Action        Pass
  Interface     ICMPGroup
  Protocol      ICMP
  ICMP type     Echo Request
  Source        any
  Destination   any
  Description   Allow ICMP echo request messages

  Option        Value
  ------------- --------------------------------
  Action        Pass
  Interface     ICMPGroup
  Protocol      ICMP
  ICMP type     Echo Reply
  Source        any
  Destination   any
  Description   Allow ICMP echo reply messages

  Option        Value
  ------------- ---------------------------------------------
  Action        Pass
  Interface     ICMPGroup
  Protocol      ICMP
  ICMP type     Destination Unreachable
  Source        any
  Destination   any
  Description   Allow ICMP destination unreachable messages

  Option        Value
  ------------- -----------------------------------
  Action        Pass
  Interface     ICMPGroup
  Protocol      ICMP
  ICMP type     Time Exceeded
  Source        any
  Destination   any
  Description   Allow ICMP time exceeded messages

If you want to use ICMP but block using it for certain networks such as your management VLAN or other sensitive networks, you could add "block" rules above the "allow" rules. I do not want to repeat all of them but you would simply add rules such as this:

  Option        Value
  ------------- ---------------------------------------------------------
  Action        Block
  Interface     ICMPGroup
  Protocol      ICMP
  ICMP type     Echo Request
  Source        any
  Destination   MGMT net
  Description   Block ICMP echo request messages to the management VLAN

WAN Rules
---------

Below are some scenarios for creating firewall rules for your WAN interface:

### Allow remote access on WAN to VPN server on OPNsense

Since the OpenVPN service is hosted on the OPNsense router, you can add the following rule to the WAN interface. This holds true for any service running on your router since you do not need to forward any ports to a device inside your network:

  Option             Value
  ------------------ --------------------------------
  Action             Pass
  Interface          WAN
  Protocol           UDP
  Source             any
  Source Port        any
  Destination        WAN address
  Destination Port   1194
  Description        Allow remote access to OpenVPN

### Allow remote access to web server on VLAN 10 using NAT port forwarding

To forward ports in OPNsense, you need to go to the "Firewall \> NAT \> Port Forward" page. Creating the rule follows a similar process to other LAN/WAN rules except that you need to also specify the IP/alias and port number of the internal device on your network. This means you need to enter values for the "Redirect target IP/port" data fields.

> **Note:** When you create a NAT port forward rule, a corresponding WAN rule is automatically created on the WAN interface. The description of the rule will be the same as the port forward rule but with "NAT" prepended to the description to indicate that it was derived from a port forward rule. If you delete the NAT port forward rule, it will also delete the WAN rule because they are linked together. However, if you delete the WAN rule that was automatically generated, it will **not** delete the corresponding port forward rule. It is likely that you have just broken your port forward rule (at least for remote access). Both rules need to exist for port forwarding to function properly.
>
> If you do not see the NAT port forward rule automatically be created on the WAN interface when you apply changes, it is possible you will need to go to the "Firewall \> Settings \> Advanced" page to ensure that the "Reflection for port forwards" option is enabled.

I have noticed that many users are confused about how to configure port forwarding in OPNsense, but creating the rules are identical except for going to a different page (NAT Port Forwarding) entering values for two additional "redirect target" data fields.

  Option                 Value
  ---------------------- -----------------------------------
  Action                 Pass
  Interface              WAN
  Protocol               TCP
  Source                 any
  Source Port            any
  Destination            WAN address
  Destination Port       443 (HTTPS)
  Redirect target IP     192.168.10.10
  Redirect target port   443 (HTTPS)
  Description            Allow remote access to web server

The external port and the port on the internal device hosting the service do not have to be identical. You could externally map any port to the port on the host device. You may actually want to use a different external port for certain services such as SSH so that your SSH service does not get spammed from scans or attacks that occur most frequently on the default port. For instance, you could have external port 2022 map to the standard port 22 on your server.

Suggestions/Feedback
--------------------

If you have any suggestions for other basic firewall examples, please let me know in the comments below or on Twitter/Facebook/email so that I may include them. Keep in mind that I prefer not to include example rules where the only difference in the rules is changing a port number because I cannot include an example for every possible service in existence. I am striving to include scenario-based examples that are straightforward to configure since this is meant to be more of a quick guide than a detailed walkthrough of a specific scenario.

------------------------------------------------------------------------

-   [OPNsense](/tags/opnsense/){.tags__link .btn}

![](/img/avatar.png){.avatar width="90" height="90"}

About Dustin Casto

Dustin Casto is a home networking enthusiast with a desire to share his experience with others

[« Previous](/how-to/configure-openvpn-opnsense/){.post-nav__link}

Accessing Your Home Network with Your Very Own OpenVPN Server on OPNsense

[Next »](/how-to/disable-root-user-opnsense/){.post-nav__link}

Disable Logging into OPNsense as the Root User

Please enable JavaScript to view the [comments powered by Disqus.](https://disqus.com/?ref_noscript)

[comments powered by Disqus](https://disqus.com){.dsq-brlink}

#### Recent Posts {#recent-posts .widget__title}

-   [Backup Your Data to Backblaze B2 with Duplicati from a Proxmox LXC Container](/how-to/backup-your-data-to-backblaze-b2-with-duplicati-from-proxmox-lxc-container/){.widget__link}
-   [Allow Push Notifications from Amcrest NVR in an Isolated Network to the Amcrest View Pro iOS App](/how-to/allow-push-notifications-amcrest-view-pro-app/){.widget__link}
-   [Create an OPNsense Virtual Machine in Virtualbox for Screenshot or Evaluation Purposes](/how-to/create-opnsense-vm-in-virtualbox-for-screenshot-or-evaluation-purposes/){.widget__link}
-   [Inadvertently Caused the Amcrest Doorbell Video Stream to have a Green Tint when Using an Amcrest NVR](/not-to/inadvertently-added-green-tint-amcrest-video-doorbell-using-amcrest-nvr/){.widget__link}
-   [Build Apple AirPlay Powered Outdoor Speakers Using a Raspberry Pi](/how-to/build-airplay-powered-outdoor-speakers-using-raspberry-pi/){.widget__link}

#### Categories {#categories .widget__title}

-   [Firewalls](/categories/firewalls/){.widget__link}
-   [Modems](/categories/modems/){.widget__link}
-   [Routers](/categories/routers/){.widget__link}
-   [Security](/categories/security/){.widget__link}
-   [Servers](/categories/servers/){.widget__link}
-   [Single board computers](/categories/single-board-computers/){.widget__link}
-   [Switches](/categories/switches/){.widget__link}
-   [Website](/categories/website/){.widget__link}
-   [Wireless](/categories/wireless/){.widget__link}

#### Tags {#tags .widget__title}

[Amcrest](/tags/amcrest/ "Amcrest"){.widget-taglist__link .widget__link .btn} [Backblaze](/tags/backblaze/ "Backblaze"){.widget-taglist__link .widget__link .btn} [Cloudflare](/tags/cloudflare/ "Cloudflare"){.widget-taglist__link .widget__link .btn} [DHCP](/tags/dhcp/ "DHCP"){.widget-taglist__link .widget__link .btn} [DNS](/tags/dns/ "DNS"){.widget-taglist__link .widget__link .btn} [Docker](/tags/docker/ "Docker"){.widget-taglist__link .widget__link .btn} [Duplicati](/tags/duplicati/ "Duplicati"){.widget-taglist__link .widget__link .btn} [Email](/tags/email/ "Email"){.widget-taglist__link .widget__link .btn} [GitLab](/tags/gitlab/ "GitLab"){.widget-taglist__link .widget__link .btn} [Hugo](/tags/hugo/ "Hugo"){.widget-taglist__link .widget__link .btn} [IPv6](/tags/ipv6/ "IPv6"){.widget-taglist__link .widget__link .btn} [LACP](/tags/lacp/ "LACP"){.widget-taglist__link .widget__link .btn} [LAG](/tags/lag/ "LAG"){.widget-taglist__link .widget__link .btn} [Nextcloud](/tags/nextcloud/ "Nextcloud"){.widget-taglist__link .widget__link .btn} [OpenVPN](/tags/openvpn/ "OpenVPN"){.widget-taglist__link .widget__link .btn} [OPNsense](/tags/opnsense/ "OPNsense"){.widget-taglist__link .widget__link .btn} [Pi-hole](/tags/pi-hole/ "Pi-hole"){.widget-taglist__link .widget__link .btn} [Plex](/tags/plex/ "Plex"){.widget-taglist__link .widget__link .btn} [Portainer](/tags/portainer/ "Portainer"){.widget-taglist__link .widget__link .btn} [Proxmox](/tags/proxmox/ "Proxmox"){.widget-taglist__link .widget__link .btn} [Raspberry Pi](/tags/raspberry-pi/ "Raspberry Pi"){.widget-taglist__link .widget__link .btn} [Sensei](/tags/sensei/ "Sensei"){.widget-taglist__link .widget__link .btn} [SFP](/tags/sfp/ "SFP"){.widget-taglist__link .widget__link .btn} [SSH](/tags/ssh/ "SSH"){.widget-taglist__link .widget__link .btn} [TP-Link](/tags/tp-link/ "TP-Link"){.widget-taglist__link .widget__link .btn} [UniFi](/tags/unifi/ "UniFi"){.widget-taglist__link .widget__link .btn} [VLAN](/tags/vlan/ "VLAN"){.widget-taglist__link .widget__link .btn} [VPN](/tags/vpn/ "VPN"){.widget-taglist__link .widget__link .btn} [WireGuard](/tags/wireguard/ "WireGuard"){.widget-taglist__link .widget__link .btn}

#### Social {#social .widget-social__title .widget__title}

Facebook

Twitter

GitLab

\[email protected\]

Reddit

Brave Browser

#### Brave Browser {#brave-browser .widget__title}

Browse with privacy, earn rewards, and support this site \-- a win-win-win!

\
[![](/img/affiliates/brave-300x100-standard.png)](https://brave.com/hom210)

© 2020 Home Network Guy. Generated with [Hugo](https://gohugo.io/) and [Mainroad](https://github.com/Vimux/Mainroad/) theme.
