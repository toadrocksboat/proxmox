# Plex Setup

[Reddit Post](https://www.reddit.com/r/Proxmox/comments/9gdnkj/plex_server_container/)

1. Create a new ubuntu 18.04 container on Proxmox host.

2. Using your existing media mountpoint on the Proxmox host to create a bind mount in the container ID's config file:

    ```
    echo "mp0: /nfs/nas01,mp=/nfs/nas01" | sudo tee /etc/pve/lxc/<ID>.conf \n
    ```

3. Create custom AppArmor profile to allow NFS mounts in the container

    ```
    echo "lxc.apparmor.profile: lxc-container-default-with-nfs" | sudo tee /etc/pve/lxc/101.conf
    ```
4. The profile should look something like this:

    ```
    cat /etc/apparmor.d/lxc/lxc-default-with-nfs
   
    profile lxc-container-default-with-nfs flags=(attach_disconnected,mediate_deleted) { deny mount fstype=devpts, mount fstype=nfsd, mount fstype=rpc_pipefs, mount fstype=cgroup -> /sys/fs/cgroup/**, }
    ```
5. Enable Plex updates when you run `apt update`

    See Doc: [Enable Repo Updates of Plex](https://support.plex.tv/articles/235974187-enable-repository-updating-for-supported-linux-server-distributions/)

    ```
    echo deb https://downloads.plex.tv/repo/deb public main | sudo tee /etc/apt/sources.list.d/plexmediaserver.list
    apt install curl
    apt install gnupg2
    curl https://downloads.plex.tv/plex-keys/PlexSign.key | sudo apt-key add -
    ```
